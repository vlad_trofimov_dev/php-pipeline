<?php

use PHPUnit\Framework\TestCase;
require './app/Model.php';

class ModelTest extends TestCase {
 public function testCheckTrue(){
    $model = new Model();
    $this->assertEquals($model->greeting(), 'Hello StarSystem!');
 }
}