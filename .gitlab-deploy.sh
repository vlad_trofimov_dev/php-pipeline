#!/bin/bash
#Get servers list
set -f
#string=$DEPLOY_SERVER
#array=(${string//,/ })
#Iterate servers for deploy and pull last commit
#for i in "${!array[@]}"do    
#      echo "Deploy project on server ${array[i]}"    
#      ssh ubuntu@${array[i]} "cd /var/www/html && git pull origin master"
#done

echo $DEPLOY_SERVER
echo "$SSH_REPO_PRIVATE_KEY" > SSH_REPO_PRIVATE_KEY
echo "$KNOWN_HOSTS" >> KNOWN_HOSTS
scp -o StrictHostKeyChecking=no SSH_REPO_PRIVATE_KEY ec2-user@$DEPLOY_SERVER:~/.ssh/id_rsa
scp -o StrictHostKeyChecking=no KNOWN_HOSTS ec2-user@$DEPLOY_SERVER:~/.ssh/known_hosts
ssh -o StrictHostKeyChecking=no ec2-user@$DEPLOY_SERVER << "EOF"
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/known_hosts
cd ~/repo
if [ -d .git &>/dev/null ]; then
    git pull
else 
    git clone git@gitlab.com:vlad_trofimov_dev/php-pipeline.git .
fi
rm -rf /var/www/html && mkdir /var/www/html
cp -r . /var/www/html/
rm -rf /var/www/html/.gitlab-deploy.sh &>/dev/null
rm -rf /var/www/html/.gitlab-ci.yml &>/dev/null
chown -R ec2-user:ec2-user /var/www/html
rm -rf ~/.ssh/id_rsa
rm -rf ~/.ssh/known_hosts
EOF

#chmod 400 virginia.pem
#ssh -o StrictHostKeyChecking=no  ec2-user@ec2-3-86-92-24.compute-1.amazonaws.com "cd /var/www/html && git pull origin master"